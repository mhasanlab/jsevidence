// Constructor Pattern with prototype method

function Student (id, name, course, price){
    this.id =id;
    this.name = name;
    this.course = course;
    this.price = price;
    this.details = function(){
        return `
        ID: ${this.id}
        Name: ${this.name}
        Course: ${this.course}
        Price: ${this.price} `;
    }
}

Student.prototype.salesTax = function(tax){
    return (this.price) * (tax)
}

var student = new Student(1, 'Mainul Hasan', 'Software Development', 50000);

console.log('\t\tPrototype Method Output:\n')

console.log(`${student.details()}
\t\tSales Tax: ${student.salesTax(.10)} `)

console.log('       ======================\n');

