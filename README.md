# what is this?

Create perfect JavaScript function. This is a set of JavaScript Function where you will get knowledge how to create JS Function Properly.

# Installation

`npm i jsfunctioncrash --save`

Then...

```
function Student (id, name, course, price){
    this.id =id;
    this.name = name;
    this.course = course;
    this.price = price;
    this.details = function(){
        return `
        ID: ${this.id}
        Name: ${this.name}
        Course: ${this.course}
        Price: ${this.price} `;
    }
}

```

# Options

efuncrash have 6 type of Javascript functions

* literal Pattern Object in JavaScript

* Dynamic Object with Factory Pattern in JavaScript

* Constructor Pattern with prototype method in JavaScript

* Prototypal Inheritance in JavaScript

* Namespace in JavaScript

* Class in JavaScript

